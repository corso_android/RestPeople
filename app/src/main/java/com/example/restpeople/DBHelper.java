package com.example.restpeople;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

import java.util.HashMap;
import java.util.List;



public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "RestPeople.db";
    private static final int DATABASE_VERSION = 1;

    private Context context;
    private String CREATE_TABLE_HOST = "CREATE TABLE HOST (host TEXT, uri TEXT, port INTEGER)";;

    DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_HOST);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


}

