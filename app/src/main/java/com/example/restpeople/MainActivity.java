package com.example.restpeople;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import layout.FragmentCreate;
import layout.FragmentHome;
import layout.FragmentProfile;

public class MainActivity extends AppCompatActivity {

    BottomNavigationView navigation;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    changeFragment(new FragmentHome());
                    return true;
                case R.id.navigation_dashboard:
                    changeFragment(new FragmentCreate());
                    return true;
                case R.id.navigation_notifications:
                    changeFragment(new FragmentProfile());
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }


    public void changeFragment(android.support.v4.app.Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content, fragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(ControllerDatabase.getInstance(this).retrieveSettings()!=null){
            navigation.setSelectedItemId(R.id.navigation_home);
            moveToHome();
        }else{
            navigation.setSelectedItemId(R.id.navigation_notifications);
            moveToProfile();
        }
    }

    public void moveToHome() {
        changeFragment(new FragmentHome());
    }

    public void moveToProfile() {
        changeFragment(new FragmentProfile());
    }

}
