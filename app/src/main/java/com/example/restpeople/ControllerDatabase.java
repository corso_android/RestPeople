package com.example.restpeople;

import android.content.Context;

import java.util.List;

/**
 * Created by giacomobellazzi on 25/09/17.
 */

public class ControllerDatabase {

    private static ControllerDatabase instance;
    private DatabaseManager databaseManager;

    private ControllerDatabase(Context context) {
        databaseManager = DatabaseManager.getInstance(context);

    }

    public static synchronized ControllerDatabase getInstance(Context context) {
        if (instance == null) {
            instance = new ControllerDatabase(context);
        }
        return instance;
    }

    public void storeSettings(SettingsHost settingsHost){
        if(retrieveSettings()!=null){
            databaseManager.deleteAllItemsFromTable("host");
        }
        databaseManager.storeObject("HOST", new HostContentValues().retrieve(settingsHost));
    }

    public SettingsHost retrieveSettings(){
        List<SettingsHost> list = databaseManager.getAllHosts();
        if(list.size()!=0){
            return list.get(0);
        }else{
            return null;
        }
    }
}
