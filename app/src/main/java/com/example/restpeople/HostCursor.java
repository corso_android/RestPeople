package com.example.restpeople;


import android.database.Cursor;

public class HostCursor {

    public SettingsHost convert(Cursor cursor){
        String host = cursor.getString(cursor.getColumnIndex("host"));
        String uri = cursor.getString(cursor.getColumnIndex("uri"));
        Integer port = cursor.getInt(cursor.getColumnIndex("port"));
        return new SettingsHost(host, uri, port);
    }
}
