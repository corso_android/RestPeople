package com.example.restpeople;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;


class DatabaseManager {

    private Integer mOpenCounter = 0;
    private static DatabaseManager instance;
    private static DBHelper mDatabaseHelper;
    private SQLiteDatabase db;
    private SQLiteDatabase mDatabase;

    private DatabaseManager(Context context) {
        mDatabaseHelper = new DBHelper(context);
        db = openDatabase();
    }

    public static synchronized DatabaseManager getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseManager(context);
        }
        return instance;
    }

    private synchronized SQLiteDatabase openDatabase() {
        mOpenCounter += 1;
        if (mOpenCounter == 1) {
            mDatabase = mDatabaseHelper.getWritableDatabase();
        }
        return mDatabase;
    }

    void storeObject(String table, ContentValues values) {
        db.insert(table, null, values);
    }

    List<SettingsHost> getAllHosts() {
        List<SettingsHost> settingsHosts = new ArrayList<>();
        Cursor cursor = db.query("HOST",
                null, null, null, null, null, null, null);

        cursor.moveToFirst();
        HostCursor hostCursor = new HostCursor();
        if (cursor.getCount() != 0) {
            while (!cursor.isAfterLast()) {
                SettingsHost settingsHost = hostCursor.convert(cursor);
                settingsHosts.add(settingsHost);
                cursor.moveToNext();
            }

        }
        cursor.close();
        return settingsHosts;
    }

    void deleteAllItemsFromTable(String table) {
        db.execSQL("DELETE FROM " + table);
    }
}
