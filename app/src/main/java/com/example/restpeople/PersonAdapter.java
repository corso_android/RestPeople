package com.example.restpeople;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;


public class PersonAdapter extends BaseAdapter {


    private List<Person> people;

    private Context context;
    private static LayoutInflater inflater = null;

    public PersonAdapter(Context context, List<Person> people) {
        this.context = context;
        this.people = people;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return people.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Person person = people.get(position);
        final ViewHolder holder;
        View rowView;
        if (convertView == null) {
            rowView = inflater.inflate(R.layout.list_view_person, parent, false);
            holder = new ViewHolder(rowView, person);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
            rowView = convertView;
        }
        holder.textView1.setText(person.getName()+" "+person.getSurname());
        holder.textView2.setText(String.valueOf(person.getBirthDay()));
        return rowView;
    }

    public void update(List<Person> people) {
        this.people = people;
        notifyDataSetChanged();
    }

    private static class ViewHolder {
        TextView textView1;
        TextView textView2;
        Person person;

        public ViewHolder(View view, Person person) {
            textView1 = (TextView) view.findViewById(R.id.textView1);
            textView2 = (TextView) view.findViewById(R.id.textView2);
            this.person = person;
        }
    }
}