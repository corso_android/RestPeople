package com.example.restpeople;


import android.content.ContentValues;

public class HostContentValues {

    public ContentValues retrieve(SettingsHost settingsHost){
        ContentValues contentValues = new ContentValues();
        contentValues.put("host", settingsHost.getHost());
        contentValues.put("uri", settingsHost.getUri());
        contentValues.put("port", settingsHost.getPort());
        return contentValues;
    }

}
