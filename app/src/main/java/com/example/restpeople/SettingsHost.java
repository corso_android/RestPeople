package com.example.restpeople;



public class SettingsHost {

    private String host;
    private String uri;
    private Integer port;

    public SettingsHost(String host, String uri, Integer port) {
        this.host = host;
        this.uri = uri;
        this.port = port;
    }

    public SettingsHost() {

    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String path(){
        return "http://"+host+":"+port+"/"+uri;
    }

    @Override
    public String toString() {
        return "SettingsHost{" +
                "host='" + host + '\'' +
                ", uri='" + uri + '\'' +
                ", port=" + port +
                '}';
    }
}
