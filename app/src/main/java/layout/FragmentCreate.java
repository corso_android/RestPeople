package layout;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.example.restpeople.ControllerDatabase;
import com.example.restpeople.MainActivity;
import com.example.restpeople.Person;
import com.example.restpeople.R;
import com.example.restpeople.SettingsHost;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;


public class FragmentCreate extends Fragment {

    @Bind(R.id.editTextName)
    EditText editTextName;
    @Bind(R.id.editTextSurname)
    EditText editTextSurname;
    @Bind(R.id.buttonBirthday)
    Button buttonBirthday;
    static Person person;
    ProgressDialog progressDialog;
    SettingsHost settingsHost;


    public static FragmentCreate updatePerson(Person person){
        FragmentCreate fragmentCreate = new FragmentCreate();
        fragmentCreate.person = person;
        return fragmentCreate;
    }

    public static FragmentCreate newPerson(){
        FragmentCreate fragmentCreate = new FragmentCreate();
        return fragmentCreate;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fragment_create, container, false);
        ButterKnife.bind(this, view);
        person = new Person();
        editTextName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d("DEBUG", s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        settingsHost = ControllerDatabase.getInstance(getContext()).retrieveSettings();
        if(person!=null){
            loadPerson(person);
        }else{
            person = new Person();
        }
        return view;
    }

    private void loadPerson(Person person) {
    }

    @OnClick(R.id.buttonBirthday)
    public void onBirthDayClick(){
        final Calendar calendar = Calendar.getInstance();
        if(person.getBirthDay()!=null){
            calendar.setTimeInMillis(person.getBirthDay()*1000);
        }
        final int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        final int month = calendar.get(Calendar.MONTH);
        final int year = calendar.get(Calendar.YEAR);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(year, month, dayOfMonth);
                person.setBirthDay(calendar.getTimeInMillis()/1000);
                Date par = new Date(calendar.getTime().getTime());
                DateFormat f = DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault());
                String string = f.format(par);
                buttonBirthday.setText(string);
            }
        }, year, month, dayOfMonth);
        datePickerDialog.show();
    }

    @OnClick(R.id.buttonSave)
    public void onButtonSave(){
        if(checkForm()){
            String name = editTextName.getText().toString();
            String surname = editTextSurname.getText().toString();
            person.setName(name);
            person.setSurname(surname);
            sendRequest();
        }
    }

    private void sendRequest(){
        AsyncHttpClient client = new AsyncHttpClient();
        String json = new Gson().toJson(person);
        try {
            StringEntity entity = new StringEntity(json);
            Log.d("DEBUG", json);
            client.post(getContext(), settingsHost.path(), entity, "application/json", new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    progressDialog.dismiss();
                    MainActivity mainActivity = (MainActivity) getActivity();
                    mainActivity.moveToHome();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    progressDialog.dismiss();
                    Log.d("DEBUG", responseString);
                    MainActivity mainActivity = (MainActivity) getActivity();
                    mainActivity.moveToHome();
                }

                @Override
                public void onStart() {
                    progressDialog = ProgressDialog.show(getActivity(),
                            null,
                            "Caricamento in corso..");
                }
            });
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private boolean checkForm(){
        if(editTextName.length()==0){
            editTextName.setError("Inserisci il nome");
            editTextName.requestFocus();
            return false;
        }
        if(editTextSurname.length()==0){
            editTextSurname.setError("Inserisci il cognome");
            editTextSurname.requestFocus();
            return false;
        }
        if(person.getBirthDay()==null){
            Toast.makeText(getContext(),"Inserisci la data di nascita", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}
