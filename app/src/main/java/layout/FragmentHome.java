package layout;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.example.restpeople.ControllerDatabase;
import com.example.restpeople.Person;
import com.example.restpeople.PersonAdapter;
import com.example.restpeople.R;
import com.example.restpeople.SettingsHost;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentHome extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    SwipeRefreshLayout swipeRefreshLayout;
    ListView listView;
    private List<Person> people;
    private PersonAdapter adapter;

    ProgressDialog progressDialog;
    SettingsHost settingsHost;

    public FragmentHome() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        settingsHost = ControllerDatabase.getInstance(getContext()).retrieveSettings();
        View view = inflater.inflate(R.layout.fragment_fragment_home, container, false);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeLayout);
        listView = (ListView) view.findViewById(R.id.listView);
        swipeRefreshLayout.setOnRefreshListener(this);
        people = new ArrayList<>();
        adapter = new PersonAdapter(getContext(), people);
        listView.setAdapter(adapter);
        loadPeople();
        return view;
    }

    @Override
    public void onRefresh() {
        loadPeople();
    }

    private void loadPeople() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(settingsHost.path(), new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                progressDialog.dismiss();
                String json = response.toString();
                Gson gson = new Gson();
                people = gson.fromJson(json, new TypeToken<List<Person>>() {
                }.getType());
                adapter.update(people);
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                progressDialog.dismiss();
                Toast.makeText(getContext(),"Problema di rete...",Toast.LENGTH_LONG).show();
            }

            @Override
            public void onStart() {
                progressDialog = ProgressDialog.show(getActivity(),
                        null,
                        "Caricamento in corso..");
            }
        });
    }
}
