package layout;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.restpeople.ControllerDatabase;
import com.example.restpeople.R;
import com.example.restpeople.SettingsHost;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentProfile extends Fragment {

    @Bind(R.id.editTextURL)
    EditText editTextURL;
    @Bind(R.id.editTextURI)
    EditText editTextURI;
    @Bind(R.id.editTextPort)
    EditText editTextPort;

    ControllerDatabase controllerDatabase;
    SettingsHost settingsHost;

    public FragmentProfile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_fragment_profile, container, false);
        ButterKnife.bind(this, view);

        controllerDatabase = ControllerDatabase.getInstance(getContext());
        settingsHost = controllerDatabase.retrieveSettings();
        if(settingsHost==null){
            settingsHost = new SettingsHost();
        }else{
            loadData(settingsHost);
        }
        return view;
    }

    private void loadData(SettingsHost settingsHost) {
        editTextURL.setText(settingsHost.getHost());
        editTextURI.setText(settingsHost.getUri());
        editTextPort.setText(String.valueOf(settingsHost.getPort()));
    }

    @OnClick(R.id.save)
    public void onSave(){
        if(checkForm()){
            String host = editTextURL.getText().toString();
            String uri = editTextURI.getText().toString();
            Integer port = Integer.valueOf(editTextPort.getText().toString());
            settingsHost.setHost(host);
            settingsHost.setUri(uri);
            settingsHost.setPort(port);
            controllerDatabase.storeSettings(settingsHost);
            Log.d("DEBUG", controllerDatabase.retrieveSettings().toString());
            Toast.makeText(getContext(),settingsHost.path(), Toast.LENGTH_LONG).show();
        }
    }

    private boolean checkForm(){
        if(editTextURL.length()==0){
            editTextURL.requestFocus();
            editTextURL.setError("Inserisci l'URL");
            return false;
        }
        if(editTextURI.length() == 0){
            editTextURI.requestFocus();
            editTextURI.setError("Inserisci l'URI");
            return false;
        }
        if(editTextPort.length()==0){
            editTextPort.requestFocus();
            editTextPort.setError("Inserisci la porta");
            return false;
        }
        return true;
    }

}
